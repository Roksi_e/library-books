import { v4 as uuidv4 } from 'uuid'
import cover from '../img/cover_not_found.jpg'

export const searchCategory = [
	'all caregory',
	'philosophy',
	'language',
	'art',
	'drama',
	'biography',
	'fiction',
	'science',
	'technology',
	'engineering',
	'religion',
	'history',
	'poetry',
	'political',
	'mind',
	'spirit',
	'music',
	'medicine',
	'business',
	'economic',
	'criticism',
	'education'
]

export const searchSort = ['relevance', 'newest']

export const createBookWithID = book => {
	if (!book.volumeInfo.categories) {
		book.categories = ['history']
	} else {
		book.categories = book.volumeInfo.categories
	}
	return {
		...book,
		categories: book.volumeInfo.categories ? book.volumeInfo.categories : ['history'],
		title: book.volumeInfo.title,
		authors: book.volumeInfo.authors ? book.volumeInfo.authors : ['unknown'],
		publishedDate: book.volumeInfo.publishedDate ? book.volumeInfo.publishedDate : 'unknown',
		img: book?.volumeInfo?.imageLinks?.thumbnail
			? `http://books.google.com/books/content?id=${book.id}&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api`
			: cover,
		ISBN: book.volumeInfo.industryIdentifiers ? book.volumeInfo.industryIdentifiers : ['unknown'],
		embeddable: book.accessInfo.embeddable,
		isFavorite: false,
		isReading: false,
		unicId: uuidv4()
	}
}

export const authorSplit = arr => {
	const item = arr.authors.map(author => {
		return author
	})
	const author = item.toString().split(',')
	return <strong>{author[0]}</strong>
}

export const randonBook = array => {
	let i = Math.floor(Math.random() * array.length)
	return array[i]
}

export const validateISBN = value => {
	const regex = /\D/
	return regex.test(value)
}
