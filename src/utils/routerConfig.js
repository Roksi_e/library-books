import { createBrowserRouter } from 'react-router-dom'
import Root from '../components/routes/root'
import ErrorPage from '../error-page'
import MainPages from '../components/pages/mainPages/MainPages'
import CollectionsPages from '../components/pages/collectionsPages/CollectionsPages'
import FavoritePages from '../components/pages/favoritePages/FavoritePages'
import CardBook from '../components/cardBook/CardBook'
import SaveCardBook from '../components/cardBook/SaveCardBook'
import BookViewer from '../components/bookViewer/BookViewer'

export const router = createBrowserRouter([
	{
		path: '/',
		element: <Root />,
		errorElement: <ErrorPage />,
		children: [
			{
				path: '/',
				element: <MainPages />
			},
			{
				path: '/collections',
				element: <CollectionsPages />
			},

			{
				path: '/favorite',
				element: <FavoritePages />
			},
			{
				path: '/favorite/reading/:id',
				element: <BookViewer />
			},
			{
				path: '/collections/reading/:id',
				element: <BookViewer />
			},
			{
				path: '/reading/:id',
				element: <BookViewer />
			}
		]
	},
	{
		path: 'books/:id',
		element: <CardBook />
	},

	{
		path: '/collections/books/:id',
		element: <SaveCardBook />
	},
	{
		path: '/favorite/books/:id',
		element: <SaveCardBook />
	}
])
