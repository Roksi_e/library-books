import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'
import { setError } from './errorSlice'
import { createBookWithID } from '../../utils/functions'

const initialState = {
	books: [],
	saveBooks: [],
	searches: {
		searchValue: 'time',
		searchCategory: '',
		searchSort: 'relevance',
		startIndex: 0
	},
	totalItems: '',
	isLoading: false
}

export const fetchBook = createAsyncThunk('books/fetchBook', async (url, thunkAPI) => {
	try {
		const res = await axios.get(url)
		return res.data
	} catch (error) {
		thunkAPI.dispatch(setError(error.message))
		return thunkAPI.rejectWithValue(error)
	}
})

const booksSlice = createSlice({
	name: 'books',
	initialState,
	reducers: {
		addBook: (state, action) => {
			state.saveBooks.push(action.payload)
		},
		deleteBook: (state, action) => {
			return {
				...state,
				saveBooks: state.saveBooks.filter(book => book.id !== action.payload)
			}
		},
		toggleFavorite: (state, action) => {
			state.saveBooks.forEach(book => {
				if (book.id === action.payload) {
					book.isFavorite = !book.isFavorite
				}
			})
		},
		addReading: (state, action) => {
			state.saveBooks.forEach(book => {
				if (book.id === action.payload) {
					book.isReading = true
				}
			})
		},
		bookSearch: (state, action) => {
			return {
				...state,
				searches: { ...state.searches, searchValue: action.payload }
			}
		},
		bookSearchCategory: (state, action) => {
			if (action.payload === 'all caregory') {
				return {
					...state,
					searches: { ...state.searches, searchCategory: '' }
				}
			} else {
				return {
					...state,
					searches: { ...state.searches, searchCategory: action.payload }
				}
			}
		},
		bookSearchSort: (state, action) => {
			return {
				...state,
				searches: { ...state.searches, searchSort: action.payload }
			}
		}
	},
	extraReducers: builder => {
		builder.addCase(fetchBook.pending, state => {
			state.isLoading = true
		})
		builder.addCase(fetchBook.fulfilled, (state, action) => {
			state.isLoading = false
			if (state.saveBooks.length !== 0 && action.payload.totalItems !== 0) {
				const double = state.saveBooks.map(book => book.id)
				const correctBooks = action.payload.items.filter(book => !double.includes(book.id))
				const finallyBooks = correctBooks.map(book => createBookWithID(book))
				state.books = finallyBooks
				state.totalItems = action.payload.totalItems
			} else if (action.payload.totalItems !== 0) {
				const finallyBooks = action.payload.items.map(book => createBookWithID(book))
				state.books = finallyBooks
				state.totalItems = action.payload.totalItems
			} else {
				setError('Немає данних')
				return state
			}
		})
		builder.addCase(fetchBook.rejected, state => {
			state.isLoading = false
		})
	}
})

export const { addBook, deleteBook, toggleFavorite, bookSearch, bookSearchCategory, bookSearchSort, addReading } =
	booksSlice.actions

export const selectBooks = state => state.books.books

export const selectSaveBooks = state => state.books.saveBooks

export const selectIsLoading = state => state.books.isLoading

export const selectSearches = state => state.books.searches

export const selectTotalItems = state => state.books.totalItems

export default booksSlice.reducer
