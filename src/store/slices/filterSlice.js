import { createSlice } from '@reduxjs/toolkit'

const initialState = {
	title: '',
	author: '',
	category: '',
	publishedYears: '',
	embeddable: ''
}

const filterSlice = createSlice({
	name: 'filter',
	initialState,
	reducers: {
		setTitleFilter: (state, action) => {
			state.title = action.payload
		},
		setAuthorFilter: (state, action) => {
			state.author = action.payload
		},
		setCategoryFilter: (state, action) => {
			state.category = action.payload
		},
		setPublishedYears: (state, action) => {
			state.publishedYears = action.payload
		},
		setEmbeddableFilter: (state, action) => {
			state.embeddable = action.payload
		},
		resetFilters: () => {
			return initialState
		}
	}
})

export const {
	setTitleFilter,
	resetFilters,
	setAuthorFilter,
	setCategoryFilter,
	setPublishedYears,
	setEmbeddableFilter
} = filterSlice.actions

export const selectTitleFilter = state => state.filter.title

export const selectAuthorFilter = state => state.filter.author

export const selectCategoryFilter = state => state.filter.category

export const selectPublishedYears = state => state.filter.publishedYears

export const selectEmbeddableFilter = state => state.filter.embeddable

export default filterSlice.reducer
