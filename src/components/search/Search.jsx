import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { LuSearch } from 'react-icons/lu'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import InputGroup from 'react-bootstrap/InputGroup'
import { bookSearch, bookSearchCategory, bookSearchSort, selectSearches } from '../../store/slices/booksSlice'
import { resetFilters } from '../../store/slices/filterSlice'
import { searchCategory, searchSort } from '../../utils/functions'
import { setError } from '../../store/slices/errorSlice'

import './Search.css'

const Search = () => {
	const [value, setValue] = useState('')
	const searches = useSelector(selectSearches)
	const dispatch = useDispatch()

	const handleBookSearchChange = e => {
		setValue(e.target.value)
	}
	const handleSearchCategoryChange = e => {
		dispatch(bookSearchCategory(e.target.value))
	}
	const handleSearchSortChange = e => {
		dispatch(bookSearchSort(e.target.value))
	}

	const handleBookSearch = value => {
		if (value) {
			const searchValue = value.replace(/[\s\W]/g, '+')
			dispatch(resetFilters())
			dispatch(bookSearch(searchValue))
			setValue('')
		} else {
			dispatch(setError('Please enter something...'))
		}
	}

	return (
		<div className="search_wrapper">
			<div className="book_search">
				<InputGroup className="mb-3 ">
					<Form.Control
						placeholder="Search books"
						aria-label="Search books"
						aria-describedby="basic-addon2"
						value={value}
						onChange={handleBookSearchChange}
					/>
					<Button variant="outline-secondary" id="button-addon2">
						<LuSearch className="icon" onClick={() => handleBookSearch(value)} />
					</Button>
				</InputGroup>
			</div>
			<div className="selectWrapper">
				<div className="select_title">Sort by:</div>
				<div className="select_wrapper">
					<Form.Select
						data-bs-theme="dark"
						defaultValue={searches.searchCategory}
						onChange={handleSearchCategoryChange}
					>
						{searchCategory.map(category => {
							return (
								<option key={category} value={category}>
									{category}
								</option>
							)
						})}
					</Form.Select>
					<Form.Select defaultValue={searches.searchSort} data-bs-theme="dark" onChange={handleSearchSortChange}>
						{searchSort.map(sort => {
							return (
								<option key={sort} value={sort}>
									{sort}
								</option>
							)
						})}
					</Form.Select>
				</div>
			</div>
		</div>
	)
}

export default Search
