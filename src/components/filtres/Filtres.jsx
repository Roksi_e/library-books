import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { v4 as uuidv4 } from 'uuid'
import Button from 'react-bootstrap/Button'
import {
	selectTitleFilter,
	resetFilters,
	setTitleFilter,
	setAuthorFilter,
	selectAuthorFilter,
	selectCategoryFilter,
	setCategoryFilter,
	selectPublishedYears,
	setPublishedYears,
	selectEmbeddableFilter,
	setEmbeddableFilter
} from '../../store/slices/filterSlice'

import './Filtres.css'

function Filtres({ books }) {
	const [categories, setCategories] = useState([])
	const [published, setPublished] = useState([])
	const [embedded, setEmbedded] = useState([])
	const dispatch = useDispatch()
	const titleFilter = useSelector(selectTitleFilter)
	const authorFilter = useSelector(selectAuthorFilter)
	const categoryFilter = useSelector(selectCategoryFilter)
	const publishedYearsFilter = useSelector(selectPublishedYears)
	const embeddableFilter = useSelector(selectEmbeddableFilter)

	useEffect(() => {
		const category = [...new Set(books.map(book => book.categories[0]))]
		setCategories(category)
		const date = [...new Set(books.map(book => book.publishedDate.split('-')[0]))]
		setPublished(date)
		const embeddable = [...new Set(books.map(book => book.embeddable.toString()))]
		setEmbedded(embeddable)
	}, [books])

	const handleTitleFilterChange = e => {
		dispatch(setTitleFilter(e.target.value))
	}

	const handleAuthorFilterChange = e => {
		dispatch(setAuthorFilter(e.target.value))
	}

	const handleCategoryFilterChange = e => {
		dispatch(setCategoryFilter(e.target.value))
	}

	const handlePublishedYearsChange = e => {
		dispatch(setPublishedYears(e.target.value))
	}

	const handleEmbeddableFilter = e => {
		dispatch(setEmbeddableFilter(e.target.value))
	}

	const handleResetFilters = () => {
		dispatch(resetFilters())
	}

	return (
		<div className="filter">
			<div className="filter-row">
				<div className="filter-group">
					<input value={titleFilter} onChange={handleTitleFilterChange} type="text" placeholder="Filter by title" />
				</div>
				<div className="filter-group">
					<input value={authorFilter} onChange={handleAuthorFilterChange} type="text" placeholder="Filter by author" />
				</div>
				<div className="filter-group">
					<input
						value={categoryFilter}
						onChange={handleCategoryFilterChange}
						type="text"
						placeholder="Filter by category"
						list="category"
					/>
					<datalist id="category">
						{categories
							? categories.map(category => {
									return <option key={uuidv4()} value={category} />
							  })
							: null}
					</datalist>
				</div>
				<div className="filter-group">
					<input
						value={publishedYearsFilter}
						onChange={handlePublishedYearsChange}
						type="text"
						placeholder="Filter by published years"
						list="published"
					/>
					<datalist id="published">
						{published
							? published.map(date => {
									return <option key={uuidv4()} value={date} />
							  })
							: null}
					</datalist>
				</div>
				<div className="filter-group">
					<input
						value={embeddableFilter}
						onChange={handleEmbeddableFilter}
						type="text"
						placeholder="Filter by embeddable"
						list="embedded"
					/>
					<datalist id="embedded">
						{embedded
							? // eslint-disable-next-line array-callback-return
							  embedded.map(em => {
									if (em === 'true') {
										return <option key={uuidv4()} value="Free Reading" />
									} else if (em === 'false') {
										return <option key={uuidv4()} value="Only for Sale" />
									} else {
										return <option key={uuidv4()} value="All" />
									}
							  })
							: null}
					</datalist>
				</div>
				<Button variant="primary" onClick={handleResetFilters}>
					Reset Filters
				</Button>
			</div>
		</div>
	)
}

export default Filtres
