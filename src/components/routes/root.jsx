import { useState } from 'react'
import { Outlet, NavLink } from 'react-router-dom'
import { LuLayoutDashboard } from 'react-icons/lu'
import { GrClose } from 'react-icons/gr'
import { MdMenuBook } from 'react-icons/md'
import { PiBookOpenBold } from 'react-icons/pi'
import { BsBookmarkStarFill } from 'react-icons/bs'

import './root.css'

export default function Root() {
	const [nav, setNav] = useState(false)

	return (
		<>
			<div className={nav ? 'sidebar active' : 'sidebar'}>
				<nav>
					<ul className="navItem">
						<li>
							<NavLink to={`/`} onClick={() => setNav(false)}>
								<LuLayoutDashboard className="book_icon" />
								Dashboard
							</NavLink>
						</li>
						<li>
							<NavLink to={`/collections`} onClick={() => setNav(false)}>
								<PiBookOpenBold className="book_icon" />
								My Collections
							</NavLink>
						</li>
						<li>
							<NavLink to={`favorite`} onClick={() => setNav(false)}>
								<BsBookmarkStarFill className="book_icon" />
								Favourites
							</NavLink>
						</li>
					</ul>
				</nav>
				<div className={nav ? 'mobile_btn open' : 'mobile_btn'} onClick={() => setNav(!nav)}>
					{nav ? <GrClose /> : <MdMenuBook />}
				</div>
			</div>
			<div id="detail">
				<Outlet />
			</div>
		</>
	)
}
