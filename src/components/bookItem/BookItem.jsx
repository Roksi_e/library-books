import { authorSplit } from '../../utils/functions'

import './BookItem.css'

const BookItem = ({ book }) => {
	return (
		<>
			<div className="book-item">
				<div className="book-item-img">
					<img src={book.img} alt={book.title} />
				</div>
				<div className="book-item-info">
					<div className="book-item-info-item title ">
						<span>{book.title}</span>
					</div>
				</div>
				<div className="book-item-info-item">
					<div className="book-item-info-item author ">
						<span>{authorSplit(book)}</span>
					</div>
				</div>
			</div>
		</>
	)
}

export default BookItem
