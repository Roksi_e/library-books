import { Link, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import Button from 'react-bootstrap/Button'
import { selectBooks, addBook, selectSaveBooks } from '../../store/slices/booksSlice'
import { authorSplit } from '../../utils/functions'
import { setError } from '../../store/slices/errorSlice'
import Error from '../Error/Error'

import './CardBook.css'

const CardBook = () => {
	const { id } = useParams()
	const books = useSelector(selectBooks)
	const saveBooks = useSelector(selectSaveBooks)
	const dispatch = useDispatch()

	const book = books.find(book => {
		return book.id === id
	})

	const isbn = book.ISBN.map(item => item.identifier).join(', ')

	const handleAddBook = book => {
		const saveBookIds = saveBooks.map(saveBook => saveBook.id)
		if (saveBookIds.includes(book.id)) {
			dispatch(setError('Book has already been added to the collection'))
		} else {
			dispatch(addBook(book))
		}
	}

	return (
		<div className="book_info_wrapper">
			<h1>{book.title}</h1>
			<div className="book_card">
				<div className="book_card_top">
					<div className="book_card_img">
						<img src={book.img} alt={book.title} />
					</div>
					<div className="book_card_list">
						<ul className="book_card_info">
							<li>Author:</li>
							<li>{authorSplit(book)}</li>
							<li>Published Date:</li>
							<li>
								<strong>{book.publishedDate}</strong>
							</li>
							<li>Categories:</li>
							<li>
								<strong>{book.categories[0]}</strong>
							</li>
							<li>Publisher:</li>
							<li>
								<strong>{book?.volumeInfo.publisher}</strong>
							</li>
							<li>Page Count:</li>
							<li>
								<strong>{book?.volumeInfo.pageCount}</strong>
							</li>
							<li>Language:</li>
							<li>
								<strong>{book?.volumeInfo.language}</strong>
							</li>
							<li>ISBN:</li>
							<li>
								<strong>{isbn}</strong>
							</li>
						</ul>
					</div>
				</div>

				<div className="book_card_content">
					<h3>Description:</h3>
					<div>{book?.volumeInfo?.description}</div>
				</div>
				<div className="book_info_btn">
					<Link to={'/'}>
						<Button variant="primary" className="button">
							Go Back
						</Button>
					</Link>
					<Button variant="primary" className="button" onClick={() => handleAddBook(book)}>
						Add Book
					</Button>
				</div>
			</div>
			<Error />
		</div>
	)
}

export default CardBook
