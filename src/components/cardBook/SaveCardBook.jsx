import { useState, useEffect } from 'react'
import { Link, useParams, useLocation } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import Button from 'react-bootstrap/Button'
import { selectSaveBooks, addReading } from '../../store/slices/booksSlice'
import { authorSplit, validateISBN } from '../../utils/functions'

import './CardBook.css'

const SaveCardBook = () => {
	const { id } = useParams()
	const [isAvailable, setIsAvailable] = useState(true)
	const location = useLocation()
	const saveBooks = useSelector(selectSaveBooks)
	const dispatch = useDispatch()

	const book = saveBooks.find(book => {
		return book.id === id
	})

	useEffect(() => {
		if (book.ISBN[0].identifier === undefined || validateISBN(book.ISBN[0].identifier)) {
			setIsAvailable(false)
		} else {
			setIsAvailable(true)
		}
	}, [book.ISBN])

	const path = location.pathname.split('/')

	const isbn = book.ISBN.map(item => item.identifier).join(', ')

	const handleReadBook = id => {
		dispatch(addReading(id))
	}

	return (
		<div className="book_info_wrapper">
			<h1>{book.title}</h1>
			<div className="book_card">
				<div className="book_card_top">
					<div className="book_card_img">
						<img src={book.img} alt={book.title} />
					</div>
					<div className="book_card_list">
						<ul className="book_card_info">
							<li>Author:</li>
							<li>{authorSplit(book)}</li>
							<li>Published Date:</li>
							<li>
								<strong>{book.publishedDate}</strong>
							</li>
							<li>Categories:</li>
							<li>
								<strong>{book.categories[0]}</strong>
							</li>
							<li>Publisher:</li>
							<li>
								<strong>{book?.volumeInfo.publisher}</strong>
							</li>
							<li>Page Count:</li>
							<li>
								<strong>{book?.volumeInfo.pageCount}</strong>
							</li>
							<li>Language:</li>
							<li>
								<strong>{book?.volumeInfo.language}</strong>
							</li>
							<li>ISBN:</li>
							<li>
								<strong>{isbn}</strong>
							</li>
						</ul>
					</div>
				</div>

				<div className="book_card_content">
					<h3>Description:</h3>
					<div>{book?.volumeInfo?.description}</div>
				</div>
				<div className="book_info_btn">
					<Link to={`/${path[1]}`} relative="path">
						<Button variant="primary">Go Back</Button>
					</Link>
					<Link
						to={isAvailable ? `/${path[1]}/reading/${book.id}` : book?.volumeInfo?.previewLink}
						target={isAvailable ? '_self' : '_blank'}
					>
						<Button variant={isAvailable ? 'primary' : 'secondary'} onClick={() => handleReadBook(book.id)}>
							Read now
						</Button>
					</Link>
				</div>
			</div>
		</div>
	)
}

export default SaveCardBook
