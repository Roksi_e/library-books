import { useRef, useState, useEffect } from 'react'
import { useParams, useLocation, useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { FaSpinner } from 'react-icons/fa'
import Button from 'react-bootstrap/Button'
import { selectSaveBooks } from '../../store/slices/booksSlice'
import { setError } from '../../store/slices/errorSlice'
import Error from '../Error/Error'

import './BookViewer.css'

const BookViewer = () => {
	const { id } = useParams()
	const [loaded, setLoaded] = useState(false)
	const location = useLocation()
	const navigate = useNavigate()
	const saveBooks = useSelector(selectSaveBooks)
	const dispatch = useDispatch()
	const canvasRef = useRef()

	const isbn = saveBooks.find(book => book.id === id).ISBN[0].identifier
	const path = location.pathname.split('/')

	const reloadPage = () => {
		console.log(window.google.books)
		window.location.reload()
	}

	function alertNotFound() {
		alert('could not embed the book!')
	}

	function alertInitialized() {
		alert('book successfully loaded and initialized!')
	}

	useEffect(() => {
		if (document.getElementById('book_script')) return
		const scriptTag = document.createElement('script')
		scriptTag.src = 'https://www.google.com/books/jsapi.js'
		scriptTag.addEventListener('load', () => setLoaded(true))
		scriptTag.id = 'book_script'
		document.body.appendChild(scriptTag)
	}, [])

	useEffect(() => {
		if (!loaded) return
		else {
			if (isbn === undefined) {
				dispatch(setError('Sorry, but this book cannot be embed'))
			} else {
				window.google.books.load()
				window.google.books.setOnLoadCallback(() => {
					let viewer = new window.google.books.DefaultViewer(canvasRef.current)
					viewer.load(`ISBN:${isbn}`, alertNotFound, alertInitialized)
					viewer.resize()
				})
			}
		}
	}, [dispatch, isbn, loaded])

	return (
		<div className="read_wrapper">
			<div className="read_btn">
				<Button variant="primary" onClick={() => navigate(`${path[1] === 'reading' ? '/' : '/' + path[1]}`)}>
					Go Back
				</Button>
				<Button variant="primary" onClick={reloadPage}>
					Load for reading
				</Button>
			</div>

			<div>
				{loaded ? (
					<div>
						<div ref={canvasRef} id="viewerCanvas"></div>
					</div>
				) : (
					<div className="read_content">
						<FaSpinner className="spinner" />
					</div>
				)}
			</div>
			<Error />
		</div>
	)
}

export default BookViewer
