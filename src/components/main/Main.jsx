import { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { PiHandsClapping } from 'react-icons/pi'
import { GiSpellBook } from 'react-icons/gi'
import { selectSaveBooks, addReading } from '../../store/slices/booksSlice'
import { Link } from 'react-router-dom'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import { randonBook, validateISBN } from '../../utils/functions'

import './Main.css'

const Main = () => {
	const saveBooks = useSelector(selectSaveBooks)
	const [randomReadBook, setRandomReadBook] = useState()
	const [randomShowBook, setRandomShowBook] = useState()
	const [isAvailable, setIsAvailable] = useState(true)
	const dispatch = useDispatch()

	useEffect(() => {
		const readBooks = saveBooks.filter(book => book.isReading)
		const showBooks = saveBooks.filter(book => !book.isReading)
		setRandomReadBook(randonBook(readBooks))
		setRandomShowBook(randonBook(showBooks))
	}, [saveBooks])

	useEffect(() => {
		if (randomShowBook !== undefined) {
			if (randomShowBook.ISBN[0].identifier === undefined || validateISBN(randomShowBook.ISBN[0].identifier)) {
				setIsAvailable(false)
			} else {
				setIsAvailable(true)
			}
		}
		if (randomReadBook !== undefined) {
			if (randomReadBook.ISBN[0].identifier === undefined || validateISBN(randomReadBook.ISBN[0].identifier)) {
				setIsAvailable(false)
			} else {
				setIsAvailable(true)
			}
		}
	}, [randomReadBook, randomShowBook])

	const handleReadBook = id => {
		dispatch(addReading(id))
	}

	return (
		<>
			{saveBooks.length === 0 ? (
				<div className="collection_empty">Your collection is still empty</div>
			) : (
				<div className="main_container">
					<div className="showCard">
						{randomShowBook !== undefined ? (
							<>
								<Card className="bg-dark text-white" style={{ width: '25rem' }}>
									<Card.Img src={randomShowBook.img} alt={randomShowBook.title} />
									<Card.ImgOverlay>
										<div className="showCard_bottom">
											<div className="showCard_body">
												<Card.Title>{randomShowBook.authors[0]}</Card.Title>
												<Card.Text>{randomShowBook.title}</Card.Text>
											</div>

											<Link
												to={isAvailable ? `reading/${randomShowBook.id}` : randomShowBook?.volumeInfo?.previewLink}
												target={isAvailable ? '_self' : '_blank'}
											>
												<Button
													variant={isAvailable ? 'primary' : 'secondary'}
													onClick={() => handleReadBook(randomShowBook.id)}
												>
													Read now
												</Button>
											</Link>
										</div>
									</Card.ImgOverlay>
								</Card>
							</>
						) : (
							<div className="mainBook_empty">
								<PiHandsClapping size="5rem" />
								<h3>You're reading all the books</h3>
							</div>
						)}
					</div>
					<div className="readBook_form">
						{randomReadBook !== undefined ? (
							<>
								<h3>Continue reading</h3>
								<Link
									to={isAvailable ? `reading/${randomReadBook.id}` : randomReadBook?.volumeInfo?.previewLink}
									target={isAvailable ? '_self' : '_blank'}
								>
									<Card style={{ width: '14rem' }} bg="dark">
										<Card.Img variant="top" src={randomReadBook.img} />
										<Card.Body>
											<Card.Text>
												<span className="readBook_title">{randomReadBook.title}</span>
											</Card.Text>
										</Card.Body>
									</Card>
								</Link>
							</>
						) : (
							<div className="mainBook_empty">
								<GiSpellBook size="5rem" />
								<h3>Choose something to read</h3>
							</div>
						)}
					</div>
				</div>
			)}
		</>
	)
}

export default Main
