import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { v4 as uuidv4 } from 'uuid'
import { FaSpinner } from 'react-icons/fa'
import { BsEmojiTear } from 'react-icons/bs'
import Button from 'react-bootstrap/Button'
import {
	addBook,
	fetchBook,
	selectBooks,
	selectIsLoading,
	selectSaveBooks,
	selectSearches,
	selectTotalItems
} from '../../store/slices/booksSlice'
import {
	selectTitleFilter,
	selectCategoryFilter,
	selectAuthorFilter,
	selectPublishedYears,
	selectEmbeddableFilter
} from '../../store/slices/filterSlice'
import { setError } from '../../store/slices/errorSlice'
import { BASE_URL, KEY_API } from '../../utils/urlConfig'
import { Link } from 'react-router-dom'
import BookItem from '../bookItem/BookItem'

import './BookList.css'

const BookList = () => {
	const [pageIndex, setPageIndex] = useState(0)
	const [count, setCount] = useState(0)
	const books = useSelector(selectBooks)
	const saveBooks = useSelector(selectSaveBooks)
	const totalItems = useSelector(selectTotalItems)
	const titleFilter = useSelector(selectTitleFilter)
	const authorFilter = useSelector(selectAuthorFilter)
	const categoryFilter = useSelector(selectCategoryFilter)
	const publishedYearsFilter = useSelector(selectPublishedYears)
	const embeddableFilter = useSelector(selectEmbeddableFilter)
	const searches = useSelector(selectSearches)
	const isLoading = useSelector(selectIsLoading)
	const dispatch = useDispatch()

	useEffect(() => {
		dispatch(
			fetchBook(
				`${BASE_URL}?q=${searches.searchValue}+subject:${searches.searchCategory}&printType=books&download=epub&orderBy=${searches.searchSort}&startIndex=${searches.startIndex}&maxResults=30&key=${KEY_API}`
			)
		)
	}, [dispatch, searches.searchCategory, searches.searchSort, searches.searchValue, searches.startIndex])

	const filterBooks = books.filter(book => {
		const embedded = () => {
			if (embeddableFilter === 'Free Reading') {
				return book.embeddable
			} else if (embeddableFilter === 'Only for Sale') {
				return !book.embeddable
			} else {
				return book
			}
		}
		const date = book.publishedDate.split('-')
		const matchTitle = book.title.toLowerCase().includes(titleFilter.toLowerCase())
		const matchAuthor = book.authors[0].toLowerCase().includes(authorFilter.toLowerCase())
		const matchCategory = book.categories[0].toLowerCase().includes(categoryFilter.toLowerCase())
		const matchPublishedYears = date[0].includes(publishedYearsFilter)
		const matchEmbedded = embedded()
		return matchTitle && matchAuthor && matchCategory && matchPublishedYears && matchEmbedded
	})

	const handleAddBook = book => {
		const saveBookIds = saveBooks.map(saveBook => saveBook.id)
		if (saveBookIds.includes(book.id)) {
			dispatch(setError('Book has already been added to the collection'))
		} else {
			dispatch(addBook(book))
		}
	}

	const handleStartIndexLoadMore = () => {
		const startIndex = pageIndex + 30
		setPageIndex(startIndex)
		if (startIndex * (count + 1) < totalItems) {
			dispatch(
				fetchBook(
					`${BASE_URL}?q=${searches.searchValue}+subject:${searches.searchCategory}&printType=books&download=epub&orderBy=${searches.searchSort}&startIndex=${startIndex}&maxResults=30&key=${KEY_API}`
				)
			)
		} else {
			dispatch(setError(`Currently there's nothing`))
		}
	}

	const handleStartIndexBack = () => {
		const startIndex = pageIndex - 30
		setPageIndex(startIndex)
		dispatch(
			fetchBook(
				`${BASE_URL}?q=${searches.searchValue}+subject:${searches.searchCategory}&printType=books&download=epub&orderBy=${searches.searchSort}&startIndex=${startIndex}&maxResults=30&key=${KEY_API}`
			)
		)
	}

	return (
		<div className="books_wrapper">
			<h2>Popular books</h2>
			<div className="content">
				{isLoading ? (
					<div className="books_spinner">
						<FaSpinner className="spinner" />
					</div>
				) : books ? (
					filterBooks?.map(book => {
						return (
							<div className="book_form" key={uuidv4()}>
								<BookItem book={book} />
								{book.embeddable ? (
									<div className="embeddable green">Free Reading</div>
								) : (
									<div className="embeddable red">Only for Sale</div>
								)}
								<div className="bottom_btn">
									<Link to={`books/${book.id}`}>
										<Button variant="primary">See more</Button>
									</Link>
									<Button variant="primary" onClick={() => handleAddBook(book)}>
										Add Book
									</Button>
								</div>
							</div>
						)
					})
				) : (
					<>
						<p>Sorry... We couldn't find anything</p>
						<BsEmojiTear size="3rem" />
					</>
				)}
			</div>
			<div className="cardBook_bottom">
				<Button
					disabled={totalItems > 0 && count !== 0 ? false : true}
					onClick={() => {
						setCount(count - 1)
						handleStartIndexBack()
					}}
				>
					Go Back
				</Button>
				<Button
					disabled={totalItems > 0 && (pageIndex + 1) * (count + 1) < totalItems ? false : true}
					onClick={() => {
						setCount(count + 1)
						handleStartIndexLoadMore()
					}}
				>
					Load more
				</Button>
			</div>
		</div>
	)
}

export default BookList
