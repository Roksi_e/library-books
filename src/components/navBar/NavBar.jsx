import { LuLayoutDashboard } from 'react-icons/lu'
import { PiBookOpenBold } from 'react-icons/pi'
import { BsBookmarkStarFill } from 'react-icons/bs'

import './NavBar.css'

const NavBar = () => {
	return (
		<>
			<div className="navItem">
				<LuLayoutDashboard className="book_icon" />
				Dashboard
			</div>
			<div className="navItem">
				<PiBookOpenBold className="book_icon" />
				My Collections
			</div>
			<div className="navItem">
				<BsBookmarkStarFill className="book_icon" />
				Favourites
			</div>
		</>
	)
}

export default NavBar
