import { useState } from 'react'
import { GrClose } from 'react-icons/gr'
import { useSelector } from 'react-redux'
import Button from 'react-bootstrap/Button'
import Error from '../../Error/Error'
import Main from '../../main/Main'
import Search from '../../search/Search'
import Filtres from '../../filtres/Filtres'
import { selectBooks } from '../../../store/slices/booksSlice'
import BookList from '../../bookList/BookList'

import './MainPages.css'

const MainPages = () => {
	const books = useSelector(selectBooks)
	const [isVisible, setIsVisible] = useState(false)

	const handleOpenModal = () => {
		setIsVisible(!isVisible)
	}

	return (
		<div className="mainWrapper">
			<div className="filter_btn">
				<Button variant="primary" onClick={handleOpenModal} disabled={!books.length ? true : false}>
					Filtres
				</Button>
				{isVisible ? (
					<>
						<GrClose className="icon_close" onClick={handleOpenModal} />
						<Filtres books={books} />
					</>
				) : null}
			</div>
			<Search />
			<Main />
			<BookList />
			<Error />
		</div>
	)
}

export default MainPages
