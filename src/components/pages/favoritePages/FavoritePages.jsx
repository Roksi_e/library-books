import { useEffect, useState } from 'react'
import { GrClose } from 'react-icons/gr'
import { useSelector } from 'react-redux'
import Button from 'react-bootstrap/Button'
import { selectSaveBooks } from '../../../store/slices/booksSlice'
import {
	selectTitleFilter,
	selectCategoryFilter,
	selectAuthorFilter,
	selectPublishedYears
} from '../../../store/slices/filterSlice'
import SaveBooksCard from '../../saveBooksCard/SaveBooksCard'
import Filtres from '../../filtres/Filtres'

import './FavoritePages.css'

const FavoritePages = () => {
	const [isVisible, setIsVisible] = useState(false)
	const [favoriteBook, setFavoriteBook] = useState([])
	const saveBooks = useSelector(selectSaveBooks)
	const titleFilter = useSelector(selectTitleFilter)
	const authorFilter = useSelector(selectAuthorFilter)
	const categoryFilter = useSelector(selectCategoryFilter)
	const publishedYearsFilter = useSelector(selectPublishedYears)

	useEffect(() => {
		const favorite = saveBooks.filter(f => {
			return f.isFavorite === true
		})
		setFavoriteBook(favorite)
	}, [saveBooks])

	const handleOpenModal = () => {
		setIsVisible(!isVisible)
	}

	const filterBooks = favoriteBook.filter(book => {
		const date = book.publishedDate.split('-')
		const matchTitle = book.title.toLowerCase().includes(titleFilter.toLowerCase())
		const matchAuthor = book.authors[0].toLowerCase().includes(authorFilter.toLowerCase())
		const matchCategory = book.categories[0].toLowerCase().includes(categoryFilter.toLowerCase())
		const matchPublishedYears = date[0].includes(publishedYearsFilter)
		return matchTitle && matchAuthor && matchCategory && matchPublishedYears
	})

	return (
		<div className="favoriteWrapper">
			<div className="filter_btn">
				<Button variant="primary" onClick={handleOpenModal} disabled={!favoriteBook.length ? true : false}>
					Filtres
				</Button>
				{isVisible ? (
					<>
						<GrClose className="icon_close" onClick={handleOpenModal} />
						<Filtres books={favoriteBook} />
					</>
				) : null}
			</div>
			<div className="favorite_filter_block"></div>
			<h1>My Favorite</h1>
			<div className="favorite_wrapper">
				<div className="favorite_content">
					{favoriteBook.length === 0 ? (
						<div className="collection_empty">You don't have any favorite books yet</div>
					) : (
						filterBooks.map(book => {
							return (
								<div className="book_form" key={book.unicId}>
									<SaveBooksCard style={{ width: '16rem' }} book={book} />
								</div>
							)
						})
					)}
				</div>
			</div>
		</div>
	)
}

export default FavoritePages
