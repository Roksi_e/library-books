import { useState } from 'react'
import { useSelector } from 'react-redux'
import { GrClose } from 'react-icons/gr'
import Button from 'react-bootstrap/Button'
import { selectSaveBooks } from '../../../store/slices/booksSlice'
import {
	selectTitleFilter,
	selectCategoryFilter,
	selectAuthorFilter,
	selectPublishedYears
} from '../../../store/slices/filterSlice'
import SaveBooksCard from '../../saveBooksCard/SaveBooksCard'
import Filtres from '../../filtres/Filtres'

import './CollectionsPages.css'

const CollectionsPages = () => {
	const [isVisible, setIsVisible] = useState(false)
	const saveBooks = useSelector(selectSaveBooks)
	const titleFilter = useSelector(selectTitleFilter)
	const authorFilter = useSelector(selectAuthorFilter)
	const categoryFilter = useSelector(selectCategoryFilter)
	const publishedYearsFilter = useSelector(selectPublishedYears)

	const handleOpenModal = () => {
		setIsVisible(!isVisible)
	}

	const filterBooks = saveBooks.filter(book => {
		const date = book.publishedDate.split('-')
		const matchTitle = book.title.toLowerCase().includes(titleFilter.toLowerCase())
		const matchAuthor = book.authors[0].toLowerCase().includes(authorFilter.toLowerCase())
		const matchCategory = book.categories[0].toLowerCase().includes(categoryFilter.toLowerCase())
		const matchPublishedYears = date[0].includes(publishedYearsFilter)
		return matchTitle && matchAuthor && matchCategory && matchPublishedYears
	})

	return (
		<div className="collectionWrapper">
			<div className="filter_btn">
				<Button variant="primary" onClick={handleOpenModal} disabled={!saveBooks.length ? true : false}>
					Filtres
				</Button>
				{isVisible ? (
					<>
						<GrClose className="icon_close" onClick={handleOpenModal} />
						<Filtres books={saveBooks} />
					</>
				) : null}
			</div>
			<div className="collection_filter_block"></div>
			<h1>My Collections</h1>
			<div className="collectionBooks_wrapper">
				<div className="collection_content">
					{saveBooks.length === 0 ? (
						<div className="collection_empty">You have not selected anything yet</div>
					) : (
						filterBooks.map(book => {
							return (
								<div className="book_form" key={book.unicId}>
									<SaveBooksCard book={book} />
								</div>
							)
						})
					)}
				</div>
			</div>
		</div>
	)
}

export default CollectionsPages
