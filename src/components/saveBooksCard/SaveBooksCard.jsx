import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { BsBookmarkStar, BsBookmarkStarFill } from 'react-icons/bs'
import Button from 'react-bootstrap/Button'
import { toggleFavorite, deleteBook, addReading } from '../../store/slices/booksSlice'
import { useEffect, useState } from 'react'
import BookItem from '../bookItem/BookItem'
import { validateISBN } from '../../utils/functions'

import './SaveBooksCard.css'

const SaveBooksCard = ({ book }) => {
	const [isAvailable, setIsAvailable] = useState(true)
	const dispatch = useDispatch()

	useEffect(() => {
		if (book.ISBN[0].identifier === undefined || validateISBN(book.ISBN[0].identifier)) {
			setIsAvailable(false)
		} else {
			setIsAvailable(true)
		}
	}, [book.ISBN])

	const handleDeleteBook = id => {
		dispatch(deleteBook(id))
	}

	const handletTogglefavorite = id => {
		dispatch(toggleFavorite(id))
	}

	const handleReadBook = id => {
		dispatch(addReading(id))
	}

	return (
		<>
			<span onClick={() => handletTogglefavorite(book.id)}>
				{book.isFavorite ? <BsBookmarkStarFill className="star-icon" /> : <BsBookmarkStar className="star-icon" />}
			</span>
			<BookItem book={book} />
			<div className="collection_bottom_btn">
				<Link to={`books/${book.id}`}>
					<Button size="sm" variant="primary">
						See more
					</Button>
				</Link>
				<Link
					to={isAvailable ? `reading/${book.id}` : book?.volumeInfo?.previewLink}
					target={isAvailable ? '_self' : '_blank'}
				>
					<Button size="sm" variant={isAvailable ? 'primary' : 'secondary'} onClick={() => handleReadBook(book.id)}>
						Read now
					</Button>
				</Link>
				<Button size="sm" variant="primary" onClick={() => handleDeleteBook(book.id)}>
					Delete Book
				</Button>
			</div>
		</>
	)
}

export default SaveBooksCard
